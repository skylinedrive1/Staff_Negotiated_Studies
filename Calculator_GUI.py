# __author__ = 'Glenn Sheils'
# __name__ = GUI_Challenge.py
# __version = Python 3.6.2

# Write a GUI program to create a simple calculator
# layout that looks like the screenshot.
#
# Try to be as Pythonic as possible - it's ok if you
# end up writing repeated Button and Grid statements,
# but consider using lists and a for loop.
#
# There is no need to store the buttons in variables.
#
# As an optional extra, refer to the documentation to
# work out how to use minsize() to prevent your window
# from being shrunk so that the widgets vanish from view.
#
# Hint: You may want to use the widgets .winfo_height() and
# winfo_width() methods, in which case you should know that
# they will not return the correct results unless the window
# has been forced to draw the widgets by calling its .update()
# method first.
#
# If you are using Windows you will probably find that the
# width is already constrained and can't be resized too small.
# The height will still need to be constrained, though.


import tkinter

# mainwindow definition
mainWindow = tkinter.Tk()
mainWindow.title("Calculator")
mainWindow.geometry('640x480+8+200')
mainWindow['padx'] = 8

# rows and columns
mainWindow.columnconfigure(0, weight=1)
mainWindow.columnconfigure(1, weight=1)
mainWindow.columnconfigure(2, weight=1)
mainWindow.columnconfigure(3, weight=1)
mainWindow.rowconfigure(0, weight=1)
mainWindow.rowconfigure(1, weight=1)
mainWindow.rowconfigure(2, weight=1)
mainWindow.rowconfigure(3, weight=1)
mainWindow.rowconfigure(4, weight=1)
mainWindow.rowconfigure(5, weight=1)

# Screen
screen = tkinter.Entry(mainWindow)
screen.grid(row=0, column=0, columnspan=1, sticky='nsew')

keyPad = tkinter.Frame(mainWindow)
keyPad.grid(row=1, column=0, sticky='nsew')

# Buttons
CButton = tkinter.Button(keyPad, text="C")
CEButton = tkinter.Button(keyPad, text="CE")
CButton.grid(row=1, column=0, sticky='nsew')
CEButton.grid(row=1, column=1, sticky='nsew')
sevenButton = tkinter.Button(keyPad, text="7")
sevenButton.grid(row=2, column=0, sticky='nsew')
eightButton = tkinter.Button(keyPad, text="8")
eightButton.grid(row=2, column=1, sticky='nsew')
nineButton = tkinter.Button(keyPad, text="9")
nineButton.grid(row=2, column=2, sticky='nsew')
plusButton = tkinter.Button(keyPad, text="+")
plusButton.grid(row=2, column=3, sticky='nsew')
fourButton = tkinter.Button(keyPad, text="4")
fourButton.grid(row=3, column=0, sticky='nsew')
fiveButton = tkinter.Button(keyPad, text="5")
fiveButton.grid(row=3, column=1, sticky='nsew')
sixButton = tkinter.Button(keyPad, text="6")
sixButton.grid(row=3, column=2, sticky='nsew')
minusButton = tkinter.Button(keyPad, text="-")
minusButton.grid(row=3, column=3, sticky='nsew')
oneButton = tkinter.Button(keyPad, text="1")
oneButton.grid(row=4, column=0, sticky='nsew')
twoButton = tkinter.Button(keyPad, text="2")
twoButton.grid(row=4, column=1, sticky='nsew')
threeButton = tkinter.Button(keyPad, text="3")
threeButton.grid(row=4, column=2, sticky='nsew')
multButton = tkinter.Button(keyPad, text="*")
multButton.grid(row=4, column=3, sticky='nsew')
zeroButton = tkinter.Button(keyPad, text="0")
zeroButton.grid(row=5, column=0, sticky='nsew')
equalsButton = tkinter.Button(keyPad, text="=")
equalsButton.grid(row=5, column=1, columnspan=2, sticky='nsew')
divideButton = tkinter.Button(keyPad, text="/")
divideButton.grid(row=5, column=3, sticky='nsew')

mainWindow.update()
mainWindow.minsize(keyPad.winfo_width() + 8, screen.winfo_height() + keyPad.winfo_height())
mainWindow.maxsize(keyPad.winfo_width() + 50 + 8, screen.winfo_height() + 50 + keyPad.winfo_height())
mainWindow.mainloop()
