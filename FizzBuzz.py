# __author__ = 'Glenn Sheils'



i: int
for i in range(1, 101):
    if i % 15 == 0:
        print(str(i) + " fizzbuzz")
        continue
    if i % 3 == 0:
        print(str(i) + " fizz")
        continue
    if i % 5 == 0:
        print(str(i) + " buzz")
        continue
    else:
        print(i)
