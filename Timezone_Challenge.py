#__author__ = 'Glenn Sheils'


# Create a program that allows a user to choose one of
# up to 9 time zones from a menu. You can choose any
# zones you want from the all_timezones list.
#
# The program will then display the time in that timezone, as
# well as local time and UTC time.
#
# Entering 0 as the choice will quit the program.
#
# Display the dates and times in a format suitable for the
# user of your program to understand, and include the
# timezone name when displaying the chosen time.

import datetime
from pytz import timezone

zones = {1: "Canada/Newfoundland", 2: "Canada/Atlantic", 3: "Canada/Eastern", 4: "Canada/Central", 5: "Canada/Mountain", 6: "Canada/Pacific", 7: "Canada/Yukon", 8: "Europe/London", 9: "Europe/Paris", 10: "Europe/Rome"}

choice = "11"
r = range(1, 12)
while True:
    if choice == 0:
        break
    for x in zones:
        print(x, ": ", zones[x])
    print()
    choice = int(input("Please enter a number between 1 and 10 to see that time zone; 0 to quit. "))
    if choice in r:
        print()
        for_tz = timezone(zones[choice])
        for_time = datetime.datetime.now(tz=for_tz)
        print("The time in " + zones[choice] + " is: " + for_time.strftime('%A %x %X %z'),for_time.tzname())
        print()
        loc_tz = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
        loc_time = datetime.datetime.now(loc_tz)
        print("The time in " + loc_time.strftime("%Z") + " is: " + loc_time.strftime('%A %x %X'))
        print()
        utc_time = datetime.datetime.utcnow()
        print("The time in UTC is: " + utc_time.strftime('%A %x %X'))
        print()