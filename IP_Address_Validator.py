#__author__ = 'Glenn Sheils'


# Create a program that takes an IP address entered at the keyboard
# and prints out the number of segments it contains, and the length of each segment.
#
# An IP address consists of 4 numbers, separated from each other with a full stop. But
# your program should just count however many are entered
# Examples of the input you may get are:
#    127.0.0.1
#    .192.168.0.1
#    10.0.123456.255
#    172.16
#    255
#
# So your program should work even with invalid IP Addresses. We're just interested in the
# number of segments and how long each one is.
#
# Once you have a working program, here are some more suggestions for invalid input to test:
#    .123.45.678.91
#    123.4567.8.9.
#    123.156.289.10123456
#    10.10t.10.10
#    12.9.34.6.12.90
#    '' - that is, press enter without typing anything
#
# This challenge is intended to practise for loops and if/else statements, so although
# you could use other techniques (such as splitting the string up), that's not the
# approach we're looking for here.


# get number from user
ipAddress = input("What is the number? ")
# Initialize variables
total_length = 0
groups = 1
segment_len = 0
ip_value = []
seg_value = []
if ipAddress == '':
    print('Please enter some numbers!')
    raise SystemExit
for character in ipAddress:  # Main Loop
    if character.isalpha():  # Test for letter
        print('Numbers only please!')
        raise SystemExit
    elif character.isdigit():  # Test if valid number
        total_length += 1
        segment_len += 1
        ip_value.append(character)
    elif character == "." and total_length == 0:  # Test if leading '.'
        print('You have a leading dot!')
        raise SystemExit

    else:  # only runs in case of "."
        # determine if valid IP segment...IE less than 255
        if segment_len == 3:
            ip_value[0] = int(ip_value[0]) * 100
            ip_value[1] = int(ip_value[1]) * 10
            seg_value = ip_value[0] + ip_value[1] + int(ip_value[2])
        if segment_len == 2:
            ip_value[0] = int(ip_value[0]) * 10
            seg_value = ip_value[0] + int(ip_value[1])
        if segment_len == 1:
            seg_value = int(ip_value[0])
        if seg_value == []:
            print('You have a null segment!')
            raise SystemExit
        if int(seg_value) >= 255:
            print('You have a value greater than 255 in a segment!')
            raise SystemExit
        print("Segment {} contains {} characters, with a value of {}".format(groups, segment_len, seg_value))  # Print segment by segment
        groups += 1
        segment_len = 0
        ip_value = []
        seg_value = []
    if segment_len >= 4:  # Error if too many digits in any segment
        print('You have more than 3 digits in one group!')
        raise SystemExit

if groups >= 5:  # Error if too many groups
    print('ERROR! You have more than 5 groups!')
    raise SystemExit
if segment_len == 0:  # Error if a trailing dot
    print('You have a trailing dot!')
    raise SystemExit


if segment_len == 3:  # Necessary to print out final segment
    ip_value[0] = int(ip_value[0]) * 100
    ip_value[1] = int(ip_value[1]) * 10
    seg_value = ip_value[0] + ip_value[1] + int(ip_value[2])
if segment_len == 2:
    ip_value[0] = int(ip_value[0]) * 10
    seg_value = ip_value[0] + int(ip_value[1])
if segment_len == 1:
    seg_value = int(ip_value[0])
if int(seg_value) >= 255:
    print('You have a value greater than 255 in a segment!')
    raise SystemExit
print("Segment {} contains {} characters, with a value of {}".format(groups, segment_len, seg_value))  # Print final segment
print("Total numbers: {0}".format(total_length))
print("Total groups: {0}".format(groups))
